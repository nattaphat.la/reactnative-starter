/**
*
* Logout
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Container, Text } from 'native-base';
import LogoutForm from '../../containers/logoutForm';
class Logout extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Container>
        <LogoutForm />
      </Container>
    );
  }
}

Logout.propTypes = {

};

export default Logout;
