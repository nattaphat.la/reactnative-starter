/**
*
* Profile
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Title,
  Right,
  Content,
} from 'native-base';
import { StatusBar } from 'react-native'; 
import ShowProfile from '../../containers/showProfile';

class Profile extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static navigationOptions = ({navigation}) => ({
    headerMode: 'screen',
    header: (
      <Header iosStatusbar="light-content"
      androidStatusBarColor='#000'>
        <StatusBar barStyle="light-content"/>
        <Left>
          <Button transparent onPress={() => navigation.navigate('DrawerOpen')}>
            <Icon name="menu" />
          </Button>
        </Left>
        <Body>
          <Title>Profile</Title>
        </Body>
        <Right />
      </Header>
    )
  })
  render() {
    return (
      <Container>
        <Content padder>
          <ShowProfile />
        </Content>
      </Container>
    );
  }
}

Profile.propTypes = {

};

export default Profile;
