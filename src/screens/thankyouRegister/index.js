/**
*
* Login
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Transition } from 'react-navigation-fluid-transitions';
// import { Col, Grid } from 'react-native-easy-grid';
import { ImageBackground, StatusBar, View } from 'react-native';
import { Container, Button, Text } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import AuthHero from '../../containers/authHero';
import TangmoBackground from '../../components/tangmoBackground';
import styles from './styles';

const launchscreenBg = require('../../../assets/launchscreen-bg.png');
const launchscreenLogo = require('../../../assets/logo.png');

class ThankyouRegister extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      navigation
    } = this.props;
    return (
      <Container>
        <StatusBar barStyle='light-content' />
        <TangmoBackground>
          <Grid>
            <Row style={{ alignItems: 'center' }}>
              <Col>
                <View style={{padding: 20}}>
                  <Text style={{ alignSelf: 'center', color: 'white' }}>Thank you for your registration</Text>
                  <Text style={{ alignSelf: 'center', color: 'white' }}>Please check your email</Text>
                  <Button success style={{ alignSelf: 'center', marginTop: 30 }} onPress={() => navigation.navigate('Login')}>
                    <Text>Login</Text>
                  </Button>
                </View>
              </Col>
            </Row>
          </Grid>
        </TangmoBackground>
        {
          // <ImageBackground source={launchscreenBg} style={styles.background}>
          //   <Content>
          //     <View>
          //       <Image style={{width: 100, height: 100}} source={{launchscreenLogo}} />
          //     </View>
          //     <View style={{ flex: 1}}>
          //       <LoginForm/>
          //     </View>
          //   </Content>
          // </ImageBackground>
        }
      </Container>
    );
  }
}

ThankyouRegister.propTypes = {

};

export default ThankyouRegister;
