/**
*
* Login
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import LoginForm from '../../containers/loginForm';
// import { Col, Grid } from 'react-native-easy-grid';
import { View } from 'react-native';
import { Container, Button, Text, Content } from 'native-base';
import TangmoBackground from '../../components/tangmoBackground';
import AuthHero from '../../containers/authHero';

class Login extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      navigation
    } = this.props;
    return (
      <Container>
        <TangmoBackground>
          <AuthHero />
          <View style={{padding: 20}}>
            <LoginForm />
            <Button transparent light style={{ alignSelf: 'center' }} onPress={() => navigation.navigate('Register')}>
              <Text>Create new account</Text>
            </Button>
          </View>
        </TangmoBackground>
        {
          // <ImageBackground source={launchscreenBg} style={styles.background}>
          //   <Content>
          //     <View>
          //       <Image style={{width: 100, height: 100}} source={{launchscreenLogo}} />
          //     </View>
          //     <View style={{ flex: 1}}>
          //       <LoginForm/>
          //     </View>
          //   </Content>
          // </ImageBackground>
        }
      </Container>
    );
  }
}

Login.propTypes = {

};

export default Login;
