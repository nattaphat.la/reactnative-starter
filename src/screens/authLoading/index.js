/**
*
* AuthLoading
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Container } from 'native-base';
import AuthLoader from '../../containers/authLoader';

class AuthLoading extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Container>
        <AuthLoader {...this.props} />
      </Container>
    );
  }
}

AuthLoading.propTypes = {

};

export default AuthLoading;
