/**
*
* Register
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import RegisterForm from '../../containers/registerForm';
// import { Col, Grid } from 'react-native-easy-grid';
import { Transition } from 'react-navigation-fluid-transitions';
import { ImageBackground, StatusBar, View } from 'react-native';
import { Container, Button, Text, Content } from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import TangmoBackground from '../../components/tangmoBackground';
import AuthHero from '../../containers/authHero';
import styles from './styles';

class Register extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      navigation
    } = this.props;
    return (
      <Container>
        <StatusBar barStyle='light-content' />
        <TangmoBackground>
          <AuthHero />
          <View style={{ padding: 20 }}>
            <RegisterForm/>
            <Button transparent light style={{ alignSelf: 'center' }} onPress={() => navigation.navigate('Login')}>
              <Text>Already have an account</Text>
            </Button>
          </View>
        </TangmoBackground>
        {
          // <ImageBackground source={launchscreenBg} style={styles.background}>
          //   <Content>
          //     <View>
          //       <Image style={{width: 100, height: 100}} source={{launchscreenLogo}} />
          //     </View>
          //     <View style={{ flex: 1}}>
          //       <RegisterForm/>
          //     </View>
          //   </Content>
          // </ImageBackground>
        }
      </Container>
    );
  }
}

Register.propTypes = {

};

export default Register;
