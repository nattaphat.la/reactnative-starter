/**
*
* TextField
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
class FunctionField extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      record,
      render,
    } = this.props;
    return (
      render(record)
    );
  }
}

FunctionField.propTypes = {

};

export default FunctionField;
