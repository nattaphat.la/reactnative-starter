/**
*
* ImageField
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Image } from 'react-native';
import placeHolderImage from '../../../assets/placeholder.png'
class ImageField extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      record,
      source,
      style,
      ...rest,
    } = this.props;
    return (
      <Image loadingIndicatorSource={placeHolderImage} source={{uri: record[source]}} resizeMode="contain" style={{width: null, height: 200, ...style}} {...rest} />
    );
  }
}

ImageField.propTypes = {

};

export default ImageField;
