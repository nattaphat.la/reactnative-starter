/**
*
* TextField
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Text } from 'native-base';
class TextField extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      record,
      source,
      ...rest,
    } = this.props;
    return (
      <Text {...rest}>{record[source]}</Text>
    );
  }
}

TextField.propTypes = {

};

export default TextField;
