/**
*
* BooleanInput
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { Item, Switch, Text, Label, Icon } from 'native-base';

class BooleanInput extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  renderInput({ input, label, type, meta: { touched, error, warning }, icon, keyboardType, secureTextEntry, itemStyle, inputStyle, placeholder, ...rest}){
    var hasError = false;
    if (error !== undefined){
      hasError = true;
    }
    const { onChange } = input;
    return (
      <Item error= {hasError} {...rest} style={{marginLeft:0, ...itemStyle}}>
        {label && <Label>{label}</Label>}
        {icon && <Icon active name={icon} />}
        <Switch onValueChange={onChange} {...input} keyboardType={keyboardType} secureTextEntry={secureTextEntry} style={inputStyle} placeholder={placeholder}/>
        {hasError ? <Text>{error}</Text> : <Text />}
      </Item>
    );
  }
  render(){
    const {
      source,
      validate,
      warn,
      ...rest,
    } = this.props;
    return (
      <Field
        name={source}
        component={this.renderInput}
        validate={validate}
        warn={warn}
        {...rest}
      />
    );
  }
}

BooleanInput.propTypes = {

};

export default BooleanInput;
