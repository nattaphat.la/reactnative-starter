/**
*
* TangmoBackground
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
// import { Container, Text } from 'native-base';
import { View } from 'react-native';
import { Content } from 'native-base';
import styles from './styles';
function TangmoBackground(props) {
  return (
    <Content style={{...styles.upperTangmo}}>
      <View style={{...styles.wrapper}}>
        <View style={{...styles.lowerTangmo}} />
        {props.children}
      </View>
    </Content>
  );
}

TangmoBackground.propTypes = {

};

export default TangmoBackground;
