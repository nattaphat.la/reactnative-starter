import ReactNative from 'react-native';
const { Dimensions, Platform } = ReactNative;
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const curveFactor = 2
export default {
  upperTangmo: {
    height: deviceHeight,
    backgroundColor: 'rgb(55,83,48)',
  },
  wrapper: {
    minHeight: deviceHeight,
  },
  lowerTangmo: {
    width: deviceWidth,
    height:deviceHeight * curveFactor,
    transform: [
      {
        scaleY: 1/curveFactor
      },
      {
        translateY:-(deviceHeight*(curveFactor/2) + (70*2))
      }], top: 0,
    position: 'absolute',
    backgroundColor: 'rgb(175,73,74)',
    borderBottomLeftRadius: deviceWidth/curveFactor,
    borderBottomRightRadius: deviceWidth/curveFactor
  }
};
