/**
*
* AuthHero
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Thumbnail, Button, Text } from 'native-base';
import { Grid, Col } from 'react-native-easy-grid';
import { Platform, Dimensions, PixelRatio } from 'react-native';
import { LoginButton } from 'react-native-fbsdk';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const platform = Platform.OS;
const isIphoneX =
  platform === 'ios' && deviceHeight === 812 && deviceWidth === 375;
const topPadding = platform === 'ios' ? (isIphoneX ? 32 : 8) : 0;
const logo = require('../../../assets/logo.png');

class AuthHero extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  handleFacebookLogin = async () => {
  };
  render() {
    return (
      <View style={{paddingTop: topPadding}}>
        <Thumbnail large source={logo} style={{ alignSelf: 'center', marginVertical: 25, width: 120, height: 120}}/>
        <LoginButton
          style={{alignSelf: 'center'}}
          publishPermissions={['email']}
          onLoginFinished={
            (error, result) => {
              if (error) {
                console.debug('Login failed with error: ' + error.message);
              } else if (result.isCancelled) {
                console.debug('Login was cancelled');
              } else {
                console.debug('Login was successful with permissions: ' + result.grantedPermissions)
              }
            }
          }
          onLogoutFinished={() => console.debug('User logged out')}/>
        <Grid style={{ alignItems: 'center', marginTop: 30, marginHorizontal: 50}}>
          <Col style={{ borderBottomWidth: 1, borderBottomColor: '#eeeeee'}}/>
          <Col style={{width: 30}}>
            <Text style={{ alignSelf: 'center', color: 'white'}}>OR</Text>
          </Col>
          <Col style={{ borderBottomWidth: 1, borderBottomColor: '#eeeeee'}}/>
        </Grid>
      </View>
    );
  }
}

AuthHero.propTypes = {

};

export default AuthHero;
