/**
*
* TextInput
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { Item, Input, Text, Label, Icon } from 'native-base';

class TextInput extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  renderInput({ input, label, type, meta: { touched, error, warning }, icon, keyboardType, secureTextEntry, itemStyle, inputStyle, placeholder, ...rest}){
    var hasError = false;
    if (error !== undefined){
      hasError = true;
    }
    return (
      <Item error= {hasError} {...rest} style={{marginLeft:0, paddingRight: 20, ...itemStyle}}>
        {label && <Label>{label}</Label>}
        {icon && <Icon active name={icon} />}
        <Input {...input} keyboardType={keyboardType} secureTextEntry={secureTextEntry} style={inputStyle} placeholder={placeholder}/>
        {hasError ? <Text style={{color: 'red'}}>{error}</Text> : <Text />}
      </Item>
    );
  }
  render(){
    const {
      source,
      validate,
      warn,
      ...rest,
    } = this.props;
    return (
      <Field
        name={source}
        component={this.renderInput}
        validate={validate}
        warn={warn}
        {...rest}
      />
    );
  }
}

TextInput.propTypes = {

};

export default TextInput;
