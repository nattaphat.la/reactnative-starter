import axios from 'axios';
import config from './config';
import store from '../stores';
const { API_URL } = config;
const requestHeader = () => {
  // const state = store.default.getState();
  const headers = {
    'Authorization': (store.getState().auth.accessToken || {}).id,
  };
  return headers;
}
const instance = () => {
  const ins = axios.create({
    baseURL: `${API_URL}/api`,
    headers: requestHeader(),
  });
  ins.interceptors.response.use(response => response.data, error => Promise.reject(error.response.data.error));
  return ins;
};
export default function(endpoint) {
  return {
    async create(payload) {
      return await instance().request({
        method: 'post',
        url: `${endpoint}`,
        data: payload,
      });
    },
    async update(payload) {
      const {id} = payload;
      return await instance().request({
        method: 'patch',
        url: `${endpoint}/${id}`,
        data: payload,
      });
    },
    async find(payload) {
      return await instance().request({
        method: 'get',
        url: `${endpoint}`,
        params: payload,
      });
    },
    async findById(payload) {
      const {id} = payload;
      return await instance().request({
        method: 'get',
        url: `${endpoint}/${id}`,
        params: payload,
      });
    },
    async delete(payload) {
      const {id} = payload;
      return await instance().request({
        method: 'delete',
        url: `${endpoint}/${id}`,
        params: payload,
      });
    },
    async custom(options) {
      return await instance().request(options);
    },
  };
}
export const APIURL = API_URL;

