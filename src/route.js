import React from 'react';
import { StackNavigator, SwitchNavigator, DrawerNavigator } from 'react-navigation';
import AuthHeader from './containers/authHero';
import Home from './screens/home/';
import Login from './screens/login';
import Logout from './screens/logout';
import Register from './screens/register';
import AuthLoading from './screens/authLoading';
import Profile from './screens/profile';
import EditProfile from './screens/editProfile';
import ThankyouRegister from './screens/thankyouRegister';

const ProfileStack = StackNavigator({
  Profile,
  EditProfile
});

const AppStack = DrawerNavigator({
  Home,
  Profile: ProfileStack,
  Logout
}, {
});
const AuthStack = StackNavigator({
  Register,
  Login,
},
{
  initialRouteName: 'Register',
  headerMode: 'none',
  mode: 'card',
});
const AppNavigator = SwitchNavigator(
  {
    AuthLoading,
    App: AppStack,
    Auth: AuthStack,
    ThankyouRegister,
  },
  {
    initialRouteName: 'AuthLoading',
  }
);

export default AppNavigator;
