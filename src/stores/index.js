import { init } from '@rematch/core';
import { reducer as formReducer } from 'redux-form';
import createLoadingPlugin from '@rematch/loading';
import selectPlugin from '@rematch/select';
import createReactNavigationPlugin from '@rematch/react-navigation';
import createRematchPersist from '@rematch/persist';
import storage from 'redux-persist/es/storage';

import Routes from '../route';
import * as models from './models';

const options = {};
const persistPlugin = createRematchPersist({
  storage,
  whitelist: ['auth'],
  throttle: 5000,
  version: 1,
});
const loading = createLoadingPlugin(options);
const { /* Navigator, */reactNavigationPlugin } = createReactNavigationPlugin({
  Routes,
  initialScreen: 'AuthLoading',
});
const reducers = {
  form: formReducer
}
const store = init({
  models,
  plugins: [
    loading,
    selectPlugin(),
    reactNavigationPlugin,
    persistPlugin,
  ],
  redux: {
    reducers,
  }
});
// export const select = getSelect();
export default store;
