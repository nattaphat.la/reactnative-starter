import dataProvider, {APIURL} from '../../utils/dataProvider';
import { dispatch } from '@rematch/core';
import { NavigationActions } from 'react-navigation';
import { LoginManager } from 'react-native-fbsdk';
const provider = dataProvider('/users');
console.log(LoginManager);
export const auth = {
  state: {
    data: null,
    accessToken: null,
  },
  reducers: {
    getUserInfo(state, payload) {
      return {
        ...state,
        data: payload,
        // accessToken: payload,
      };
    },
    login(state, payload) {
      return {
        ...state,
        accessToken: payload,
      };
    },
    logout(state, payload) {
      return {
        ...state,
        data: null,
        accessToken: null,
      };
    }
  },
  effects: {
    // handle state changes with impure functions.
    // use async/await for async actions
    async loginAsync(payload, rootState) {
      const result = await provider.custom({
        url: '/users/login',
        method: 'post',
        data: payload,
      });
      this.login(result);
      dispatch(NavigationActions.navigate({ routeName: 'AuthLoading' }));
    },
    async logoutAsync(payload, rootState) {
      LoginManager.logOut();
      this.logout(payload);
      dispatch(NavigationActions.navigate({ routeName: 'AuthLoading' }));
    },
    async getUserInfoAsync(payload, rootState) {
      try {
        const result = await provider.findById({
          id: 'me'
        });
        this.getUserInfo(result);
      } catch (e) {
        this.getUserInfo(null);
      }
    },
    async facebookLoginAsync(payload, rootState) {
      try {
        const result = await provider.custom({
          baseURL: APIURL,
          url: '/auth/facebook/callback',
          method: 'get',
          params: payload,
        });
        this.getTokenInfoAsync(result);
      } catch (e) {
        this.logoutAsync();
        dispatch(NavigationActions.navigate({ routeName: 'AuthLoading' }));
      }
    },
    async getTokenInfoAsync(payload, rootState) {
      try {
        const result = await provider.custom({
          url: `/users/me/accesstokens/${payload.access_token}`,
          method: 'get',
          headers: {
            Authorization: payload.access_token,
          },
        });
        this.login(result);
        dispatch(NavigationActions.navigate({ routeName: 'AuthLoading' }));
      } catch (e) {
        this.logoutAsync();
        dispatch(NavigationActions.navigate({ routeName: 'AuthLoading' }));
      }
    }
  },
  selectors: {
    isAuthenticated(state) {
      return !!state.data;
    }
  }
}