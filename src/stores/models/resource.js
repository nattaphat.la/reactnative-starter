import dataProvider from '../../utils/dataProvider';
import { Toast } from 'native-base';
export default function(model) {
  const provider = dataProvider(`/${model}`);
  const resource = {
    state: {
      data: {},
      ids: [],
      errors: null,
    },
    reducers: {
      // handle state changes with pure functions
      findById(state, payload) {
        state.data[payload.id] = payload;
        return {
          ...state,
          errors: null,
        };
      },
      find(state, payload) {
        const newData = payload.reduce((res, o) => {
          res[o.id] = o;
          return res;
        }, {});
        return {
          ...state,
          data: {
            ...state.data,
            ...newData
          },
          errors: null,
        };
      },
      delete(state, payload) {
        delete state.data[payload.id];
        state.errors = null;
        return state;
      },
      create(state, payload) {
        state.data.new = payload;
        state.errors = null;
        return state;
      },
      update(state, payload) {
        state.data[payload.id] = payload;
        state.errors = null;
        return state;
      },
      error(state, payload) {
        return {
          ...state,
          errors: payload,
        };
      }
    },
    effects: {
      // handle state changes with impure functions.
      // use async/await for async actions
      async findAsync(payload, rootState) {
        try {
          const result = await provider.find(payload);
          this.find(result);
        } catch (e) {
          console.debug(e);
          this.error(e);
          Toast.show({
            type: 'danger',
            position: 'bottom',
            text: e.message
          });
          return Promise.reject(e);
        }
      },
      async findByIdAsync(payload, rootState) {
        try {
          const result = await provider.findById(payload);
          this.findById(result);
        } catch (e) {
          console.debug(e);
          this.error(e);
          Toast.show({
            type: 'danger',
            position: 'bottom',
            text: e.message
          });
          return Promise.reject(e);
        }
      },
      async deleteAsync(payload, rootState) {
        try {
          const { id } = payload;
          this.delete({id});
          await provider.delete(payload);
        } catch (e) {
          console.debug(e);
          this.error(e);
          Toast.show({
            type: 'danger',
            position: 'bottom',
            text: e.message
          });
          return Promise.reject(e);
        }
        // TODO bring back deleted data if provider not return 200 status
        
      },
      async updateAsync(payload, rootState) {
        // update -> update store then reload from server
        try {
          const { id } = payload;
          this.update(payload);
          await provider.update(payload);
          await this.findByIdAsync({id});
        } catch (e) {
          console.debug(e);
          this.error(e);
          Toast.show({
            type: 'danger',
            position: 'bottom',
            text: e.message
          });
          return Promise.reject(e);
        }
      },
      async createAsync({loadData = true, ...payload}, rootState) {
        console.log(rootState);
        // create -> update store then reload from server
        try {
          this.create(payload);
          const result = await provider.create(payload);
          const { id } = result;
          if (loadData) {
            await this.findByIdAsync({id});
          }
          this.delete({id: 'new'});
        } catch (e) {
          console.debug(e);
          this.error(e);
          Toast.show({
            type: 'danger',
            position: 'bottom',
            text: e.message
          });
          return Promise.reject(e);
        }
      }
    }
  };
  return resource;
}

