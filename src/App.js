import React from 'react';
import { Root } from 'native-base';
import { Provider } from 'react-redux';
import createReactNavigationPlugin from '@rematch/react-navigation';
import { getPersistor } from '@rematch/persist';
import { PersistGate } from 'redux-persist/es/integration/react';

const persistor = getPersistor();

import store from './stores';
import Routes from './route';
const { Navigator } = createReactNavigationPlugin({
  Routes,
  initialScreen: 'AuthLoading',
});
export default () =>
(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <Root>
        <Navigator />
      </Root>
    </PersistGate>
  </Provider>
);
