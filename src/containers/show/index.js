/**
*
* Show
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { Text, Spinner } from 'native-base';

class Show extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    const {
      model,
      id,
    } = this.props;
    this.props.dispatch[model].findByIdAsync({id});
  }
  render() {
    const {
      state,
      model,
      id,
      children,
    } = this.props;
    if (state.loading.effects[model].findByIdAsync) {
      return (<Spinner />);
    }
    if (!state[model].data[id]) {
      return (<Text>No data</Text>);
    }
    const childrenWithProps = React.Children.map(children, child =>
      React.cloneElement(child, {
        record: state[model].data[id]
      }));
    return (
      <View {...this.props} >
        {childrenWithProps}
      </View>
    );
  }
}

Show.propTypes = {

};
const mapStateToProps = state => ({
  state,
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Show);
