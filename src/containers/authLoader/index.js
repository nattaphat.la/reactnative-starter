/**
*
* AuthLoader
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Text, Spinner } from 'native-base';
import { Col, Grid } from 'react-native-easy-grid';
import { select } from '@rematch/select';
import { View } from 'react-native';

class AuthLoader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }
  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    // const userToken = await AsyncStorage.getItem('userToken');
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    const result = await this.props.getUserInfo({});
    this.props.navigation.navigate(this.props.isAuthenticated ? 'App' : 'Auth');
  };
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#635DB7'}}>
        <Grid>
          <Col style={{ alignSelf: 'center'}}>
            <Spinner />
            <Text style={{ textAlign: 'center', color: 'white' }}>Loading profile...</Text>
          </Col>
        </Grid>
      </View>
    );
  }
}

AuthLoader.propTypes = {

};
const mapStateToProps = state => ({
  isAuthenticated: select.auth.isAuthenticated(state),
  loading: state.loading.models.auth,
});

const mapDispatchToProps = dispatch => ({
  getUserInfo: dispatch.auth.getUserInfoAsync,
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoader);
