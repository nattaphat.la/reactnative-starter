// const React = require('react-native');
// const { Dimensions, Platform } = React;
// const deviceHeight = Dimensions.get('window').height;

export default {
  // logoContainer: {
  //   flex: 1,
  //   marginTop: deviceHeight / 8,
  //   marginBottom: 30
  // },
  // logo: {
  //   position: 'absolute',
  //   left: Platform.OS === 'android' ? 40 : 50,
  //   top: Platform.OS === 'android' ? 35 : 60,
  //   width: 280,
  //   height: 100
  // },
  input: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  lastItem: {
    borderBottomWidth: 0,
  },
  item: {
    backgroundColor: 'white',
    borderRadius: 25,
    marginBottom: 10,
  },
  actionButton: {
    alignSelf: 'center',
  }
};
