/**
*
* LoginForm
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Text, Spinner } from 'native-base';
import { View } from 'react-native';
import SimpleForm from '../simpleForm';
import TextInput from '../../components/TextInput';
import styles from './styles';

class LoginForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  handleSubmit = (data) => {
    this.props.login(data);
  }
  render() {
    return (
      <SimpleForm onSubmit={this.handleSubmit} formActions={({handleSubmit}) => (
        <Button primary onPress={handleSubmit} style={styles.actionButton}>
          <Text>Login</Text>
          {this.props.loading && <Spinner />}
        </Button>
      )}>
        <View>
          <TextInput source="email" placeholder="email" keyboardType="email-address" itemStyle={styles.item} inputStyle={styles.input}/>
          <TextInput source="password" placeholder="password" secureTextEntry itemStyle={{...styles.item,...styles.lastItem}} inputStyle={styles.input}/>
        </View>
      </SimpleForm>
    );
  }
}

LoginForm.propTypes = {

};
const mapStateToProps = state => ({
  // isAuthenticated: state.auth.isAuthenticated,
  loading: state.loading.models.auth,
});

const mapDispatchToProps = dispatch => ({
  login: dispatch.auth.loginAsync
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
