/**
*
* LogoutForm
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Container, Text } from 'native-base';
import { connect } from 'react-redux';

class LogoutForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.logout();
  }
  render() {
    return (
      <Container>
        <Text>Logging out</Text>
      </Container>
    );
  }
}

LogoutForm.propTypes = {

};
const mapStateToProps = state => ({
  // isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => ({
  // login: dispatch.auth.login
  logout: dispatch.auth.logoutAsync,
});

export default connect(mapStateToProps, mapDispatchToProps)(LogoutForm);
