/**
*
* SimpleForm
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import {
  Text,
  Button,
  Form,
} from 'native-base';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { reduxForm } from 'redux-form';
class SimpleForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    props.initialize(props.record);
  }
  render() {
    const {
      handleSubmit,
      children,
      formActions,
      // reset,
    } = this.props;
    return (
      <Form>
        {children}
        <View style={{paddingVertical: 20}}>
          {
            formActions ?
            React.createElement(formActions, this.props) :
            (
              <Button block primary onPress={handleSubmit}>
                <Text>Submit</Text>
              </Button>
            )
          }
        </View>
      </Form>
    );
  }
}

SimpleForm.propTypes = {

};
const mapStateToProps = state => ({
  // isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => ({
  // login: dispatch.auth.login
  dispatch,
});
export function createForm(key){
  return compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
      form: key,
    })
  )(SimpleForm);
}
export default createForm('redux-form');
