/**
*
* Create
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Button, Text, Spinner } from 'native-base';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';

class Create extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  handleSubmit = async (data) => {
    const {
      model,
      loadData,
      afterCreate,
    } = this.props;
    try {
      await this.props.dispatch[model].createAsync({...data, loadData});
    } catch (e) {
      if (e && e.details && e.details.messages) {
        throw new SubmissionError(e.details.messages);
      }
    }
    afterCreate(data);
  }
  render() {
    const {
      children,
      createButtonLabel,
      ...rest
    } = this.props;
    return (
      React.cloneElement(children, {
        onSubmit: this.handleSubmit,
        record: {},
        formActions: ({handleSubmit}) => (
          <Button block primary onPress={handleSubmit}>
            <Text>{createButtonLabel || 'Create'}</Text>
            {this.props.loading && <Spinner />}
          </Button>
        ),
        ...rest,
      })
    );
  }
}

Create.propTypes = {

};
const mapStateToProps = state => ({
  // isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => ({
  // login: dispatch.auth.login
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(Create);
