/**
*
* ShowProfile
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Text } from 'native-base';
import { connect } from 'react-redux';
import Show from '../../containers/show';
import TextField from '../../components/textField';
import FunctionField from '../../components/functionField';
import ImageField from '../../components/imageField';
class ShowProfile extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Show model="users" id={this.props.userId}>
        <ImageField source="profileImage" />
        <FunctionField render={record => (<Text>{`${record.firstname} ${record.lastname}`}</Text>)}/>
        <TextField source="firstname"/>
      </Show>
    );
  }
}

ShowProfile.propTypes = {

};
const mapStateToProps = state => ({
  userId: state.auth.accessToken.userId,
});

const mapDispatchToProps = dispatch => ({
  // login: dispatch.auth.login
});

export default connect(mapStateToProps, mapDispatchToProps)(ShowProfile);
