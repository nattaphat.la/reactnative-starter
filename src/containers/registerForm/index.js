/**
*
* RegisterForm
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import {compose} from 'recompose';
import { Button, Text, Spinner } from 'native-base';
import { View } from 'react-native';
import { connect } from 'react-redux';
import Create from '../../containers/create';
import SimpleForm from '../../containers/simpleForm';
import TextInput from '../../components/TextInput';
import { withNavigation } from 'react-navigation';
import styles from './styles';

class RegisterForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      navigation
    } = this.props;
    return (
      <Create
        model="users"
        formActions={({handleSubmit}) => (
          <Button success onPress={handleSubmit} style={styles.actionButton}>
            <Text>Register</Text>
            {this.props.loading && <Spinner />}
          </Button>
        )}
        loadData={false}
        afterCreate={payload => navigation.navigate('ThankyouRegister')}
      >
        <SimpleForm>
          <View>
            {
              // <TextInput source="firstname" placeholder="Firstname" keyboardType="email-address" inputStyle={styles.input} itemStyle={styles.item}/>
              // <TextInput source="lastname" placeholder="Lastname" inputStyle={styles.input} itemStyle={styles.item}/>
            }
            <TextInput source="email" placeholder="Email" inputStyle={styles.input} itemStyle={styles.item}/>
            <TextInput source="password" placeholder="Password" inputStyle={styles.input} itemStyle={styles.item}/>
            <TextInput source="confirmPassword" placeholder="Confirm password" inputStyle={styles.input} itemStyle={{...styles.item,...styles.lastItem}}/>
          </View>
        </SimpleForm>
      </Create>
    );
  }
}

RegisterForm.propTypes = {

};
const mapStateToProps = state => ({
  // isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => ({
  // login: dispatch.auth.loginAsync
});

export default compose(connect(mapStateToProps, mapDispatchToProps), withNavigation)(RegisterForm);
